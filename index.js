const http = require("http");
const fs = require("fs");
const uuid = require("uuid");
const url = require("url");

const server = http.createServer((req, res) => {
  if (req.url == "/GET/html") {
    fs.readFile("./public/index.html", (err, content) => {
      if (err) {
        throw err;
      } else {
        res.end(content);
      }
    });
  }
  if (req.url == "/GET/json") {
    fs.readFile("./public/data.json", (err, content) => {
      if (err) {
        throw err;
      } else {
        res.end(content);
      }
    });
  }
  if (req.url == "/GET/uuid") {
    let uuidData = {
      uuid: uuid.v4(),
    };
    res.end(JSON.stringify(uuidData));
  }
  const parsedUrl = url.parse(req.url, true);
  const pathname = parsedUrl.pathname;
  const path = pathname.split("/");
  let code = Number(path[path.length - 1]);
  let delay = Number(path[path.length - 1]);

  if (req.url == "/GET/status/" + code) {
    res.writeHead(code, { "content-type": "text/plain" });
    res.end(JSON.stringify(http.STATUS_CODES[code]));
  }
  if (req.url == "/GET/delay/" + delay) {
    setTimeout(() => {
      res.end(JSON.stringify(http.STATUS_CODES[200]));
    }, delay * 1000);
  } else {
    fs.readFile("./public/404.html", (err, content) => {
      if (err) {
        throw err;
      } else {
        res.end(content);
      }
    });
  }
});

const PORT = 5000;

server.listen(PORT, () => {
  console.log("server is running on", PORT);
});
